===============================================================================
Thank you for using ROBOT SHOP APPLICATION, this guide will help you use the application.
===============================================================================

1. The demo executable file appears in the build folder, after you run "make" in the terminal.

2. The passwords are:
		Sales Associate: 9999
                Sales Manager:   3210
                Boss:                         9611
                Customer:              0000

3. If you want some preloaded data for your shop, go to File dropdown menu then click Open
     then open the "scr" folder and open save.txt file.

