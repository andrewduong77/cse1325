To compile full credit:
g++ -std=c++11 -o full_credit HW1_reg_Key.cpp

To run full credit
./full_credit

To compile extra credit:
g++ -std=c++11 -o extra_credit HW1_ec_Key.cpp

To run extra credit
./extra_credit