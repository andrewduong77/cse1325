To begin:
1. Extract the ZIP (.zip) file into a directory with the same name.
2. Right-click the directory and click on "Open in Terminal".

To demonstrate the main program:
1. Type in the "make clean && make all" command and press the 'ENTER' key.

To clean the directory:
1. Type in the "make clean" command and press the 'ENTER' key.
